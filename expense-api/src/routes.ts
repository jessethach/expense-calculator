import { Routes } from 'nest-router';
import { ParticipantModule } from './participant/participant.module';
import { CalculatorModule } from './calculator/calculator.module';

export const routes: Routes = [
  {
    path: '/api',
    module: CalculatorModule,
  },
  {
    path: '/api',
    module: ParticipantModule,
  },
];
