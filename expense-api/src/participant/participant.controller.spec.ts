import { Test, TestingModule } from '@nestjs/testing';
import { ParticipantController } from './participant.controller';
import { ParticipantService } from './participant.service';
import { participantMockData } from './participant-data';

const participantData = participantMockData;

describe('ParticipantController', () => {
  let controller: ParticipantController;
  let service: ParticipantService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ParticipantController],
      providers: [ParticipantService],
    }).compile();

    service = module.get<ParticipantService>(ParticipantService);
    controller = module.get<ParticipantController>(ParticipantController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('getAll', () => {
    it('should return an array of participants', async () => {
      const result = participantData;
      jest.spyOn(service, 'findAll').mockImplementation(() => result);

      expect(await controller.getAll()).toBe(result);
    });
  });
});
