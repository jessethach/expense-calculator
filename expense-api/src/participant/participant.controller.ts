import {
  Controller,
  Get,
  Post,
  Delete,
  Patch,
  Body,
  UsePipes,
  Param,
  NotFoundException,
} from '@nestjs/common';
import { ParticipantService } from './participant.service';
import { Participant } from '../models/participant.dto';
import { ValidationPipe } from '../pipes/validation.pipe';
import { PatchParticipantExpenseDto } from '../models/patch-participant-expense.dto';

@Controller('participant')
export class ParticipantController {
  constructor(private readonly pService: ParticipantService) {}

  @Get()
  async getAll() {
    return this.pService.findAll();
  }

  @Post()
  @UsePipes(new ValidationPipe())
  async addParticipant(@Body() participant: Participant) {
    return this.pService.createParticipant(participant);
  }

  @Patch()
  @UsePipes(new ValidationPipe())
  async patchParticipantExpense(
    @Body() patchExpense: PatchParticipantExpenseDto,
  ) {
    const { id, expenses } = patchExpense;
    const participant = this.pService.findById(id);

    if (!participant) {
      throw new NotFoundException('User Not Found');
    } else {
      return this.pService.patchParticipant(id, expenses);
    }
  }

  @Delete('/delete/:id')
  async removeParticipantById(@Param('id') pId: string) {
    return this.pService.deleteParticipant(pId);
  }
}
