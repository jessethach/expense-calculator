export const participantMockData = [
  {
    id: '1a',
    name: 'Adriana',
    expenses: [
      {
        activity: 'ferry',
        cost: 52,
      },
      {
        activity: 'burgers',
        cost: 37,
      },
    ],
  },
  {
    id: '1b',
    name: 'Bao',
    expenses: [
      {
        activity: 'museum',
        cost: 30,
      },
    ],
  },
  {
    id: '1c',
    name: 'Camden',
    expenses: [
      {
        activity: 'hotel',
        cost: 582,
      },
    ],
  },
];
