import { IsString, IsInt } from 'class-validator';
import { Tally } from '../models/tally';

export class CreateParticipantDto {
  @IsString() readonly id: string;
  @IsString() readonly name: string;
  @IsInt() readonly expenses: Tally[];
}
