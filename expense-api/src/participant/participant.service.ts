import { Injectable } from '@nestjs/common';
import { Tally } from '../models/tally';
import { Participant } from '../models/participant.dto';
import { participantMockData } from './participant-data';

@Injectable()
export class ParticipantService {
  private participants: Participant[] = participantMockData;

  findById(id: string) {
    return this.participants.find((p) => p.id === id);
  }

  findAll(): Participant[] {
    return this.participants;
  }

  createParticipant(participant: Participant) {
    this.participants.push(participant);
  }

  patchParticipant(id: string, expenses: Tally[]) {
    return this.participants.map((p) => {
      if (p.id === id) {
        return {
          ...p,
          expenses,
        };
      } else {
        return p;
      }
    });
  }

  deleteParticipant(id: string) {
    this.participants = this.participants.filter((p) => p.id !== id);

    return this.participants;
  }
}
