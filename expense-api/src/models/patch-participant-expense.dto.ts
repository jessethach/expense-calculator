import { IsString, IsInt } from 'class-validator';
import { Tally } from '../models/tally';

export class PatchParticipantExpenseDto {
  @IsString() readonly id: string;
  @IsInt() readonly expenses: Tally[];
}
