import { ParticipantBase } from './participant-base';
import { Tally } from './tally';

export class Participant extends ParticipantBase {
  expenses: Tally[];
}
