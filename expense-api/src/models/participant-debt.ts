import { ParticipantBase } from './participant-base';

export class ParticipantDebt extends ParticipantBase {
  debt: number;
  hidden: boolean;
}
