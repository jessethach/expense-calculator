import { Module } from '@nestjs/common';
import { CalculatorModule } from './calculator/calculator.module';
import { ParticipantModule } from './participant/participant.module';
import { RouterModule } from 'nest-router';
import { routes } from './routes';

@Module({
  imports: [
    RouterModule.forRoutes(routes),
    ParticipantModule,
    CalculatorModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
