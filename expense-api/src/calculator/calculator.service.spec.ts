import { Test, TestingModule } from '@nestjs/testing';
import { CalculatorService } from './calculator.service';
import { ParticipantService } from '../participant/participant.service';
import { participantMockData } from '../participant/participant-data';

const participantData = participantMockData;

describe('CalculatorService', () => {
  let cService: CalculatorService;
  let pService: ParticipantService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CalculatorService, ParticipantService],
    }).compile();

    cService = module.get<CalculatorService>(CalculatorService);
    pService = module.get<ParticipantService>(ParticipantService);
  });

  it('should be defined', () => {
    expect(cService).toBeDefined();
  });

  it('should calculate total', () => {
    const data = participantData;
    const expectedResult = 701;

    jest.spyOn(pService, 'findAll').mockImplementation(() => data);

    expect(cService.calculateTripTotal()).toBe(expectedResult);
  });
});
