import { Injectable } from '@nestjs/common';
import { ParticipantDebt } from '../models/participant-debt';
import { ParticipantService } from '../participant/participant.service';

@Injectable()
export class CalculatorService {
  constructor(private readonly pService: ParticipantService) {}

  calculateTripTotal() {
    const participants = this.pService.findAll();

    return participants.reduce((sum, { id }) => {
      return sum + this.calculateTotalByParticipant(id);
    }, 0);
  }

  calculateTotalByParticipant(id: string) {
    return this.pService
      .findAll()
      .find((p) => p.id === id)
      ?.expenses.reduce((acc, exp) => {
        return acc + exp.cost;
      }, 0);
  }

  calculateBetweenSelectedParticipants(selectedIds?: string[]) {
    if (!selectedIds || selectedIds?.length === 0) {
      const participants = this.pService.findAll();
      const length = participants.length;
      const participantDebt = this.calculateTripTotal() / length;

      return participants.map(({ id }) => {
        return {
          id,
          debt: participantDebt,
          hidden: false,
        };
      });
    } else {
      const participants = this.pService.findAll();
      const selectedParticipants = participants.map<ParticipantDebt>(
        ({ id, name }) => {
          if (selectedIds.includes(id)) {
            return {
              id,
              name,
              debt: 0,
              hidden: false,
            };
          } else {
            return {
              id,
              name,
              debt: 0,
              hidden: true,
            };
          }
        },
      );

      return selectedParticipants.map((p) => {
        if (!p.hidden) {
          return {
            ...p,
            debt: this.calculateParticipantDebt(p.id),
          };
        } else {
          return p;
        }
      });
    }
  }

  calculateParticipantDebt(id: string) {
    const totalParticipants = this.pService.findAll();
    const participant = totalParticipants.find((p) => p.id === id);
    if (!participant) {
      return 0;
    }
    const { expenses } = participant;

    const average = this.calculateTripTotal() / totalParticipants.length;
    const totalExpense = expenses.reduce((sum, exp) => {
      return sum + exp.cost;
    }, 0);

    return average - totalExpense;
  }
}
