import { Test, TestingModule } from '@nestjs/testing';
import { CalculatorController } from './calculator.controller';
import { CalculatorService } from './calculator.service';
import { ParticipantService } from '../participant/participant.service';
import { participantMockData } from '../participant/participant-data';

const participantData = participantMockData;

describe('CalculatorController', () => {
  let controller: CalculatorController;
  let cService: CalculatorService;
  let pService: ParticipantService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CalculatorController],
      providers: [CalculatorService, ParticipantService],
    }).compile();

    controller = module.get<CalculatorController>(CalculatorController);
    cService = module.get<CalculatorService>(CalculatorService);
    pService = module.get<ParticipantService>(ParticipantService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('calculateTripTotal', () => {
    it('should return an array of cats', async () => {
      const result = participantData;
      const totalResult = 9;

      jest.spyOn(pService, 'findAll').mockImplementation(() => result);
      jest
        .spyOn(cService, 'calculateTripTotal')
        .mockImplementation(() => totalResult);

      expect(await controller.getTotalExpense()).toBe(totalResult);
    });
  });
});
