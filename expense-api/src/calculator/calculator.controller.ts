import { Controller, Get, Query, Param } from '@nestjs/common';
import { CalculatorService } from './calculator.service';

@Controller('calculator')
export class CalculatorController {
  constructor(private readonly cService: CalculatorService) {}

  @Get()
  async getTotalExpense() {
    return this.cService.calculateTripTotal();
  }

  @Get('selected')
  async getExpenseBetweenSelected(
    @Query() { selectedIds }: { selectedIds: string[] },
  ) {
    return this.cService.calculateBetweenSelectedParticipants(selectedIds);
  }

  @Get('/:id')
  async getDebtById(@Param() id: string) {
    return this.cService.calculateTotalByParticipant(id);
  }
}
