import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule, HttpTestingController,
} from '@angular/common/http/testing';
import { Participant } from '../models/participant';
import { ParticipantService } from './participant.service';
import { HttpClient } from '@angular/common/http';
import { participantApi } from '../constants/api';

describe('ParticipantService', () => {
  let service: ParticipantService;
  let participant: Participant;
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });

    service = TestBed.inject(ParticipantService);
    httpMock = TestBed.inject(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return data', () => {
    participant = {
      id: 'testa',
      name: 'Jib',
      expenses: [
        {
          activity: 'foo',
          cost: 52,
        },
        {
          activity: 'bar',
          cost: 37,
        },
      ],
    };

    service.getParticipants().subscribe(([p]) => {
      expect(p).toEqual(participant);
    });

    const req = httpMock.expectOne(participantApi);

    expect(req.request.method).toEqual('GET');

    req.flush(participant);

    httpMock.verify();
  });
});
