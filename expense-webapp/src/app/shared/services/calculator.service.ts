import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { calculatorApi } from '../constants/api';
import { catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { ParticipantDebt } from '../models/participant-debt';

@Injectable({
  providedIn: 'root'
})
export class CalculatorService {
  private api = calculatorApi;
  // TODO: remove when selection logic in component is finished
  private selectedIds: string[] = [ '1a', '1b', '1c'];

  constructor(private http: HttpClient) { }

  getTotalTripExpense(): Observable<number> {
    return this.http.get<number>(this.api).pipe(
      catchError(this.handleError<number>(0))
    );
  }

  getExpenseBetweenSelected(
    selectedIds: string[] = this.selectedIds
    ): Observable<ParticipantDebt[]> {
    let params = new HttpParams();
    selectedIds.forEach((id) => {
      params = params.append('selectedIds', id);
    });

    return this.http.get<ParticipantDebt[]>(`${this.api}/selected`, { params }).pipe(
        catchError(this.handleError<ParticipantDebt[]>([])
      )
    );
  }

  private handleError<T>(result?: T): (e: any) => Observable<T> {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}
