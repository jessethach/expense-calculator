import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { participantApi } from '../constants/api';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Participant } from '../models/participant';

@Injectable({
  providedIn: 'root'
})
export class ParticipantService {
  private api = participantApi;

  constructor(private http: HttpClient) { }

  getParticipants(): Observable<Participant[]> {
    return this.http.get<Participant[]>(this.api).pipe(
      catchError(this.handleError<Participant[]>([]))
    );
  }

  private handleError<T>(result?: T): (e: any) => Observable<T> {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}
