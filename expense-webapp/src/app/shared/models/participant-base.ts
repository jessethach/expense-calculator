export interface ParticipantBase {
    id: string;
    name: string;
}
