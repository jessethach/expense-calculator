import { Tally } from './tally';
import { ParticipantBase } from './participant-base';

export interface Participant extends ParticipantBase {
    expenses: Tally[];
}
