export type Tally = {
  activity: string;
  cost: number;
};
