import { ParticipantBase } from './participant-base';

export interface ParticipantDebt extends ParticipantBase {
    debt: number;
    hidden: boolean;
}
