import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ParticipantService } from '../shared/services/participant.service';

@Component({
  selector: 'app-participant-form',
  templateUrl: './participant-form.component.html',
})
export class ParticipantFormComponent implements OnInit {
  addParticipantForm: FormGroup;

  constructor(private fb: FormBuilder, private pService: ParticipantService) { }

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm(): void {
    // name, expenses(array of tallies)
    this.addParticipantForm = this.fb.group({
      id: [0],
      name: [''],
      expenses: [0]
    });
  }

  submitParticipant({ value }: FormGroup): void {
    console.log(value);
  }
}
