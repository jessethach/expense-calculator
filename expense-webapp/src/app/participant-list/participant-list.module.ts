import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ParticipantListComponent } from './participant-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ParticipantFormModule } from '../participant-form/participant-form.module';

@NgModule({
  declarations: [
    ParticipantListComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    ParticipantFormModule
  ],
  exports: [ParticipantListComponent]
})
export class ParticipantListModule { }
