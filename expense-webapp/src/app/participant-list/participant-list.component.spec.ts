import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ParticipantListComponent } from './participant-list.component';
import { Tally } from '../shared/models/tally';

describe('ParticipantListComponent', () => {
  let component: ParticipantListComponent;
  let fixture: ComponentFixture<ParticipantListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, ReactiveFormsModule, FormsModule],
      declarations: [ ParticipantListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipantListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should calculate the sum of epxpenses', () => {
    const mockExpenses: Tally[] = [
      {
        activity: 'foo',
        cost: 50,
      },
      {
        activity: 'bar',
        cost: 63
      }
    ];
    const expectedTotal = 113;

    const result = component.expenseSum(mockExpenses);

    expect(result).toBe(expectedTotal);
  });

  it('should calculate the sum and return a number', () => {
    const mockExpenses: Tally[] = [
      {
        activity: 'foo',
        cost: 50,
      },
      {
        activity: 'bar',
        cost: 63
      }
    ];

    const result = component.expenseSum(mockExpenses);

    expect(result).toEqual(113);
  });
});
