import { Component, OnInit } from '@angular/core';
import { Participant } from '../shared/models/participant';
import { Tally } from '../shared/models/tally';
import { ParticipantService } from '../shared/services/participant.service';
import { Observable } from 'rxjs';
import { CalculatorService } from '../shared/services/calculator.service';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { ParticipantDebt } from '../shared/models/participant-debt';

@Component({
  selector: 'app-participant-list',
  templateUrl: './participant-list.component.html',
})
export class ParticipantListComponent implements OnInit {
  participants: Participant[] = [];
  participantDebts: ParticipantDebt[] = [];
  totalTripCost$: Observable<number>;
  filterForm: FormGroup;
  currentDetail: Participant;
  hideData = true;

  constructor(
    private pService: ParticipantService,
    private cService: CalculatorService,
    private fb: FormBuilder) { }

  ngOnInit(): void {
    this.initForm();
    this.getAllParticipants();
    this.getDebtList();
    this.totalTripCost$ = this.cService.getTotalTripExpense();
  }

  initForm(): void {
    this.filterForm = this.fb.group({
      debtList: this.fb.array([])
    });
  }

  buildForm(): void {
    this.filterForm = this.fb.group({
      debtList: this.buildFilters()
    });
  }

  getAllParticipants(): void {
    this.pService.getParticipants().subscribe(
      (participants) => {
        this.participants = participants;
      }
    );
  }

  getDebtList(): void {
    this.cService.getExpenseBetweenSelected().subscribe(
      (participantDebts) => {
        this.participantDebts = participantDebts;

        this.buildForm();
      }
    );
  }

  buildFilters(): FormArray {
    const arr = this.participantDebts.map(({ hidden }) => {
      return this.fb.group({ hidden });
    });

    return this.fb.array(arr);
  }

  getSelectedDebt(selectedId: string): void {
    this.cService.getExpenseBetweenSelected();
  }

  expenseSum(expenses: Tally[]): number {
    return expenses.reduce((acc, { cost }) => {
      return acc + cost;
    }, 0);
  }

  onClickDetails(participant: Participant): void {
    this.currentDetail = participant;
  }

  onSubmit({ value }: FormGroup): void {
    // TODO: Add logic to hit endpoint to recalculate based on selection
    console.log(value);
    this.hideData = false;
  }

  get filterFormGroups(): FormArray {
    return this.filterForm.get('debtList') as FormArray;
  }
}
