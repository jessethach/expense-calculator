import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ParticipantFormModule } from './participant-form/participant-form.module';
import { ParticipantListModule } from './participant-list/participant-list.module';
import { CalculatorService } from './shared/services/calculator.service';
import { ParticipantService } from './shared/services/participant.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    ParticipantFormModule,
    ParticipantListModule
  ],
  providers: [ParticipantService, CalculatorService],
  bootstrap: [AppComponent]
})
export class AppModule { }
