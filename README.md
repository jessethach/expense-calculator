### Important notes

There are two directories that contain the web app and an API. The `expense-api` directory contains the backend logic and the `expense-webapp` directory contains the front end logic.

The frontend is built with Angular and the backend leverages a NodeJS backend framework called [NestJS](https://nestjs.com/)

Follow the steps below to run the application.

### Getting Started

- Ensure you have [Node](https://nodejs.org/en/) installed. [NVM](https://github.com/nvm-sh/nvm) is a great tool for using different versions of Node if needed. It's useful to have two terminals open to run both the frontend and the api at the same time and they will run on `http://localhost:4200/` and `http://localhost:3000/` respectively.

- Clone this repo

  ```
  $ git clone https://bitbucket.org/jessethach/expense-calculator.git
  ```

* Step into the `expense-api` directory

  ```
  $ cd expense-api
  ```

* Install dependencies

  ```
  $ npm install
  ```

* Run the app locally

  ```
  $ npm start
  ```

* Step into the `expense-webapp` directory

  ```
  $ cd expense-webapp
  ```

* Install dependencies

  ```
  $ npm install
  ```

* Run the app locally

  ```
  $ npm start
  ```

  Then open localhost:4200/ on browser

##### Tech

- Angular/TypeScript/Node.js/Karma
- Bulma.io for CSS layout
- Angular CLI
- NestJS CLI

##### Env

Node v12.18.4
Angualr cli version 11.2.8
Angular 11.2.9
NestJS 7.6.0

#### MVP

- Create API containing calculator and participant controllers and services.
- Create frontend that will interface with the API and display data from participants.

#### Stretch Goals

- Angular routing.
- Persist data in a database.
- Form to add more participants (this is work that has been started in the participant-form directory).
- Create logic that would calculate the best way for users to distribute money and to whom.
- Concentrate more on unit testing (had to trade off in order to build a full stack solution).
- Add feature to select/deselect people and recalculate based on selection.
